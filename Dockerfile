### STAGE 1: Build ###
FROM node:lts-alpine AS builder
ENV PORT 3000
WORKDIR /src
COPY package.json .
RUN npm install
COPY . .
RUN npm run build


### STAGE 2: Run ###
FROM node:lts-alpine AS production
EXPOSE 3000/tcp
WORKDIR /app
COPY --from=builder /src/package.json .
RUN npm install
RUN npm install express morgan dotenv
COPY index.js .
COPY --from=builder /src/build ./build
CMD [ "node", "index.js" ]




# FROM node:lts-alpine AS builder
# WORKDIR /src
# COPY package.json .
# RUN npm install
# RUN npm install express morgan
# COPY . .
# RUN npm run build
# EXPOSE 3000/tcp
# CMD ["node", "App.js"]

